schleuder
=========

Ansible role to install Schleuder (version 3),

Requirements
------------
Only supported in Debian Jessie (8)

Role Variables
--------------


Dependencies
------------

Example Playbook
----------------

    - hosts: servers
      roles:
         - role: schleuder

License
-------

GPL


